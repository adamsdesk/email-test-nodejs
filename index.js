var nodemailer = require('nodemailer');

var transport = nodemailer.createTransport({
        host: 'mail.example.com',
        port: 465,
        secure: true,
        auth: {
            user: "user@example.com",
            pass: "example-password"
        }
    });

var message = {
    from: 'test@example.com',
    to: '"Webmaster" <webmaster@example.com>',
    subject: 'Nodemailer is unicode friendly ✔',
    text: 'Hello to myself!',
};

transport.sendMail(message, function(error){
    if(error){
        console.log('Error occured');
        console.log(error);
        return;
    }
    transport.close();
});